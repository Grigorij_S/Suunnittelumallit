package pakkaus;

public class Charmander implements PokeState{
	
	//LVL 1
	
	private int nextLvl = 7;

	
	@Override
	public void Attack(Pokemon p, int dmg) {
		if (dmg>0){
			p.addXP(-1);
		}else{
			p.addXP(2);
		}
		
		if (p.getXP()>nextLvl){
			Evolve(p);
		}
	}

	@Override
	public void Defend(Pokemon p, int dmg) {
		if (dmg>0){
			p.addXP(2);
		}else{
			p.addXP(1);
		}
		if (p.getXP()>nextLvl){
			Evolve(p);
		}
	}

	@Override
	public void Evolve(Pokemon p) {
		p.evolve(new Charmeleon());
	}
	
	@Override
	public void UnEvolve(Pokemon p) {
	}

	@Override
	public void Die(Pokemon p) {
		if (p.getXP()<=0){
			System.out.println("1st state");
		}else{
			p.addXP(-2);
		}
	}
}
