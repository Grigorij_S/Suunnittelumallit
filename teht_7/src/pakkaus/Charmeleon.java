package pakkaus;

public class Charmeleon implements PokeState{
	
	//LVL 2
	
	private int nextLvl = 26;
	
	@Override
	public void Attack(Pokemon p, int dmg) {
		if (dmg>0){
			p.addXP(-3);
		}else{
			p.addXP(3);
		}
		if (p.getXP()>nextLvl){
			Evolve(p);
		}
	}

	@Override
	public void Defend(Pokemon p, int dmg) {
		if (dmg>0){
			p.addXP(4);
		}else{
			p.addXP(2);
		}
		if (p.getXP()>nextLvl){
			Evolve(p);
		}
	}

	@Override
	public void Evolve(Pokemon p) {
		p.evolve(new Charizard());
		
	}
	
	@Override
	public void UnEvolve(Pokemon p) {
		p.evolve(new Charmander());
		System.out.println("DOwn to Charmander");
	}

	@Override
	public void Die(Pokemon p) {
		p.addXP(-4);
		
		if (p.getXP()<7){
			UnEvolve(p);
		}
		System.out.println("");
		System.out.println("That hurts!");
		System.out.println("");
	}
}
