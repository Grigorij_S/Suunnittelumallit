package pakkaus;

public class Pokemon {

	private PokeState state;
	private int XP;
	
	
	public Pokemon(){
		state = new Charmander();
		XP = 0;
	}
	
	public int getXP(){
		return XP;
	}
	
	public void addXP(int xp){
		if (xp>0){
			System.out.println("You've gained "+xp+"xp");
		}else{
			System.out.println("You've lost "+xp+"xp");
		}
			this.XP += xp;
	}
	
	public void attack(int dmg){
		System.out.println("");
		System.out.println("Attack!");
		state.Attack(this, dmg);
	}
	
	public void defence(int dmg){
		System.out.println("");
		System.out.println("Defend!");
		state.Defend(this, dmg);
	}
	
	protected void evolve(PokeState state){
		this.state = state;
		System.out.println("You have "+XP+"xp");
		System.out.println("Your Pokemon has evolved to "+state.getClass().getSimpleName());
		System.out.println("");
	}
}
