package pakkaus;

public class Charizard implements PokeState{
	
	//LVL 3
	
	
	@Override
	public void Attack(Pokemon p, int dmg) {
		if (dmg>0){
			p.addXP(-3);
		}else{
			if (p.getXP()<26){
				p.addXP(2);
			}
		}
	}

	@Override
	public void Defend(Pokemon p, int dmg) {
	}

	@Override
	public void Evolve(Pokemon p) {
	}
	
	@Override
	public void UnEvolve(Pokemon p) {
		p.evolve(new Charmeleon());
		System.out.println("Back to Charmeleon");
	}

	@Override
	public void Die(Pokemon p) {
		p.addXP(-8);
		
		if (p.getXP()<26){
			UnEvolve(p);
		}
		System.out.println("");
		System.out.println("That hurts!");
		System.out.println("");
	}
	
}
