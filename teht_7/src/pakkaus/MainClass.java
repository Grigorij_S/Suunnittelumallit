package pakkaus;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class MainClass {
	
	public static void main (String[] args) {
		
		Scanner s = new Scanner(System.in);
		Pokemon pokemon = new Pokemon();
		
		int action = 0, hit;
		Random rand = null;
		
		System.out.println("You are Charmander");
		System.out.println("The enemy is in front of you");
		System.out.println("What will you do?");
		
		
		while(action != 3){
			System.out.println("");
			System.out.println("1. Attack");
			System.out.println("2. Defend");
			System.out.println("3. Run away");

			rand = new Random();
			hit = rand.nextInt(6);
			
			System.out.println("rand: "+hit);
			
			if (hit>0){
				System.out.println("");
				System.out.println("You are about to be attacked!");
			}
			
			action = s.nextInt();
			
			switch(action){
			case(1):
				pokemon.attack(hit);
				break;
			case(2):
				pokemon.defence(hit);
				break;
			default:
				System.out.println("Type 1, 2 or 3");
				break;
			}
			for (int i=0;i<14;i++){
				System.out.println("");
			}
		}
	}
}
