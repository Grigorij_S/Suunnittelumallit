package pakkaus;

public interface PokeState {
	public void Attack(Pokemon p, int dmg);
	public void Defend(Pokemon p, int dmg);
	public void Evolve(Pokemon p); //change state
	public void UnEvolve(Pokemon p);
	public void Die(Pokemon p);
}
