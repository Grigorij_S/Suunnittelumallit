package teht_12;

import java.util.ArrayList;

public class ProxyExample {
	public static void main(String[] args) {
		
		ArrayList<Image> list = new ArrayList<Image>();
		
        Image IMAGE1 = new ProxyImage("HiRes_10MB_Photo1");
        Image IMAGE2 = new ProxyImage("HiRes_10MB_Photo2");
        Image IMAGE3 = new ProxyImage("HiRes_10MB_Photo3");
        
        list.add(IMAGE1);
        list.add(IMAGE2);
        list.add(IMAGE3);
        
        for(int i = 0; i < list.size(); i++){
            list.get(i).showData();
        }
	}
}
