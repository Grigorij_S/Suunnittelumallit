package teht_9;

import java.util.List;

public class Strategy2 implements ListConverter{
    
    public String listToString(List list) {
    	
        String[] strTable = new String[list.size()];
        String str = "";
        
        for(int i=0; i<list.size(); i++){
        	strTable[i] = (String)list.get(i);
        }
        
        for(int i=1; i<strTable.length+1; i++){
            if(i % 2 == 0){
            	str = str + strTable[i-1] + "\n";
            }else{
            	str = str + strTable[i-1];
            }
        }
        return str;
    }
}
