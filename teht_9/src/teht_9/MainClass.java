package teht_9;

import java.util.ArrayList;
import java.util.List;

public class MainClass {
	public static void main(String[] args) {
        List<String> list = new ArrayList();
        
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        list.add("E");
        list.add("F");
        list.add("G");
        list.add("H");
        list.add("I");
        list.add("J");
        list.add("K");
        list.add("L");
        list.add("M");
        list.add("N");
        list.add("O");
        list.add("P");
        list.add("Q");
        list.add("R");
        list.add("S");
        list.add("T");
        list.add("U");
        list.add("V");
        list.add("W");
        list.add("X");
        list.add("Y");
        list.add("Z");
        
        ListConverter lc1 = new Strategy1();
        ListConverter lc2 = new Strategy2();
        ListConverter lc3 = new Strategy3();
        
        System.out.println("LIST 1");
        System.out.println(lc1.listToString(list));
        System.out.println("");
        System.out.println("LIST 2");
        System.out.println(lc2.listToString(list));
        System.out.println("");
        System.out.println("LIST 3");
        System.out.println(lc3.listToString(list));
    }
}
