package teht_9;

import java.util.Iterator;
import java.util.List;

public class Strategy3 implements ListConverter{
	
	public String listToString(List list) {
        String str = "";
        Iterator iterator = list.iterator();
        
        while(iterator.hasNext()){
        	str = str + (String)iterator.next() + "\n";
        }
        return str;
    }

}
