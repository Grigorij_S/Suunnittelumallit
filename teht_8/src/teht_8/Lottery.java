package teht_8;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class Lottery extends Game{

	Scanner scanner = new Scanner(System.in);
	private String LotteryRow = "Lottery_Row: ", playersRow = "Your_Row:", jackPot, pot;
	private String[] splitPlayerRow;
	private int player, guessedNumbers = 0, min, max, number;
	private boolean TheEnd = false;
	private long win;

	@Override
	void initializeGame() {
		
		win = 100 + (long) (Math.random() * 10000000);
		min = 1;
		max = 100;
		for (int i=0; i<10; i++){
			number = min + (int) (Math.random() * max);
			LotteryRow = LotteryRow+" "+number;
		}
		LotteryRow = LotteryRow+" ";
	}

	@Override
	void makePlay(int player) {
		this.player = player;
		System.out.println("Pelaajan " + player + " vuoro arvata.");
		System.out.println("");
		System.out.println("Anna 10 numeroa, 1 - 100");
		
		for (int i=0; i<10; i++){
			System.out.println("Numerosi: ");
			
			int guessNumber = scanner.nextInt();
			
			if (guessNumber < 0 || guessNumber > 100){
				System.out.println("V��r� sy�te, yrit� uudestaan.");
				i--;
			}else{
				playersRow = playersRow+" "+guessNumber;
			}
		}
		splitPlayerRow = playersRow.split(" ");
		
		for (int i=1; i<splitPlayerRow.length; i++){
			if (LotteryRow.contains(" "+splitPlayerRow[i]+" ")){
				guessedNumbers++;
			}
		}
		
		jackPot = NumberFormat.getCurrencyInstance(new Locale("fi", "FI")).format(win);
		
		if (guessedNumbers != 10){
			win = win/((10 - guessedNumbers)*100000);
		}
		
		
		
		pot = NumberFormat.getCurrencyInstance(new Locale("fi", "FI")).format(win);

		if (guessedNumbers == 10){
			System.out.println("!!!   P��VOITTO   !!!");
			System.out.println("Voitit: "+pot);
		}else if (guessedNumbers == 1){
			System.out.println("Arvasit "+guessedNumbers+" numeron!");
			System.out.println("Voitit: "+pot);
		}else if (guessedNumbers>0){
			System.out.println("Arvasit "+guessedNumbers+" numeroa!");
			System.out.println("Voitit: "+pot);
		}else{
			System.out.println("Valitettavasti et arvannut yht��n numeroa =(");
			System.out.println("H�visit 10 �");
		}

		endTheGame(true);
	}

	@Override
	boolean endOfGame() {
		return TheEnd;
	}

	@Override
	void printWinner() {
		System.out.println("Rivisi on: "+playersRow.substring(9, playersRow.length()));
		System.out.println("LottoRivi oli: "+LotteryRow.substring(13, LotteryRow.length()));
		System.out.println("P��voitto oli: "+jackPot);
	}
	
	public void endTheGame(boolean TheEnd) {
		this.TheEnd = TheEnd;
	}

}
