package oliot;

import factorymethod.AterioivaOtus;
import factorymethod.Juoma;
import factorymethod.Ruoka;
import juomat.Vesi;
import ruoka.Peruna;

public class Opettaja extends AterioivaOtus {

    public Juoma createJuoma(){
        return new Vesi();
    }

    public Ruoka createRuoka() {
		return new Peruna();
	}
}
