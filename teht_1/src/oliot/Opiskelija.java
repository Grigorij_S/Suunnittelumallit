package oliot;

import factorymethod.AterioivaOtus;
import factorymethod.Juoma;
import factorymethod.Ruoka;
import juomat.Maito;
import ruoka.Piirakka;

public class Opiskelija extends AterioivaOtus {

    public Juoma createJuoma(){
        return new Maito();
    }
    
    public Ruoka createRuoka() {
		return new Piirakka();
	}

}
