package oliot;

import factorymethod.AterioivaOtus;
import factorymethod.Juoma;
import factorymethod.Ruoka;
import juomat.EnergiaJuoma;
import ruoka.Sitruuna;

public class Vartija extends AterioivaOtus {

    public Juoma createJuoma(){
        return new EnergiaJuoma();
    }

	public Ruoka createRuoka() {
		return new Sitruuna();
	}
}
