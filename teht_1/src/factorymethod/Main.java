package factorymethod;

import oliot.Opettaja;
import oliot.Opiskelija;
import oliot.Vartija;

public class Main {

    public static void main(String[] args) {
    	System.out.println("opettaja");
        AterioivaOtus opettaja = new Opettaja();
        opettaja.aterioi();
        
        System.out.println("");
        System.out.println("opiskelija");
        AterioivaOtus opiskelija = new Opiskelija();
        opiskelija.aterioi();
        
        System.out.println("");
        System.out.println("vartija");
        AterioivaOtus vartija = new Vartija();
        vartija.aterioi();
    }
}
