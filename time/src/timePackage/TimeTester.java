package timePackage;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.Before;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

@RunWith(Parameterized.class)
public class TimeTester {

   private Integer inputNumber;
   private String expectedResult;
   private TimeUtils timeUtils;

   @Before
   public void initialize() {
	   timeUtils = new TimeUtils();
   }

   // Each parameter should be placed as an argument here
   // Every time runner triggers, it will pass the arguments
   // from parameters we defined in primeNumbers() method
	
   public TimeTester(Integer inputNumber, String expectedResult) {
      this.inputNumber = inputNumber;
      this.expectedResult = expectedResult;
   }

   @Parameterized.Parameters
   public static Collection integersTimes() {
      return Arrays.asList(new Object[][] {
         { 2, "0:00:02" },
         { 66, "0:01:06" },
         { 191, "0:03:11" },
         { 222, "0:03:42" },
         { 3665, "1:01:05" }
      });
   }

   // This test will run 4 times since we have 5 parameters defined
   @Test
   public void testTimeChecker() {
      System.out.println("Integer Number is : " + inputNumber);
      assertEquals(expectedResult, timeUtils.secToTime((inputNumber)));
   }
}
