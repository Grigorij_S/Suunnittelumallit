package pizzaLaatikko;

public class Salami extends Decorator{
	
	double price = 3.99;
    String name = "Cheese";
    public Salami(PizzaInterface pizza) {
        super(pizza);
    }
    
    @Override
    public double getPrice() {
        return super.getPrice() + price;
    }
    
    @Override
    public String getName() {
        return super.getName() + name;
    }

}
