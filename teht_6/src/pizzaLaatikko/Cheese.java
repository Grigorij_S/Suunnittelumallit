package pizzaLaatikko;

public class Cheese extends Decorator{
	
	double price = 2.99;
    String name = "Cheese";
    public Cheese(PizzaInterface pizza) {
        super(pizza);
    }
    
    @Override
    public double getPrice() {
        return super.getPrice() + price;
    }
    
    @Override
    public String getName() {
        return super.getName() + name;
    }
}
