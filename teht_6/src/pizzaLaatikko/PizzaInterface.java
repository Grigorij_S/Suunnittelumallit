package pizzaLaatikko;

public interface PizzaInterface {
	public double getPrice();
	public String getName();
}
