package pizzaLaatikko;

public class PizzaPohja implements PizzaInterface{
	
	double price = 4.99;
    String name = "Pizzapohja";
    
    public double getPrice() {
        return price;
    }
    public String getName() {
        return name;
    }
}
