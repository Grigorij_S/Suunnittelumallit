package pizzaLaatikko;

public abstract class Decorator implements PizzaInterface{

	protected PizzaInterface pizza;
	
    public Decorator(PizzaInterface pizza) {
        this.pizza = pizza;
    }
    
    public double getPrice() {
        return pizza.getPrice();
    }
    public String getName() {
        return pizza.getName();
    }
}
