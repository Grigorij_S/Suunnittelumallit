package observer;

import java.util.Observable;

public class Subject extends Observable implements Runnable{
	

	ClockTimer ct;
	
	public Subject(){
		ct = new ClockTimer();
	}
	
	@Override
	public void run() {
		
		while(true){
			ct.tick();
			setChanged();
			notifyObservers(ct);
			try{
				Thread.sleep(1000);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
}
