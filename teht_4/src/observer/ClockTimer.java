package observer;

public class ClockTimer{
	
	int hour = 1, minute = 14, second = 50;
	
	void tick(){
		second++;
		if (second == 60){
			second = 0;
			minute++;
			if (minute == 60){
				minute = 0;
				hour++;
			}
		}
	}

	public int getHour() {
		return hour;
	}
	public int getMinute(){
		return minute;
	}
	public int getSecond(){
		return second;
	}

}
