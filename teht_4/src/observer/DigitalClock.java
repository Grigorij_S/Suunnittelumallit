package observer;

import java.util.Observable;
import java.util.Observer;

public class DigitalClock implements Observer {
	private ClockTimer timer;	
	
	String ss, mm, hh;

	private void draw(){
		int hour = timer.getHour();
		int minute = timer.getMinute();
		int second = timer.getSecond();
		
		
		
		if (minute < 10){
			mm = "0"+minute;
		}else{
			mm = String.valueOf(minute);
		}
		if (second < 10){
			ss = "0"+second;
		}else{
			ss = String.valueOf(second);
		}
		System.out.println(hour+":"+mm+":"+ss);
	}

	@Override
	public void update(Observable arg0, Object arg1) {

		timer = (ClockTimer)arg1;
		draw();
	}
}
