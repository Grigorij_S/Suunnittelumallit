package pakkaus;

import interFacet.Housut;
import interFacet.Kengat;
import interFacet.Lippis;
import interFacet.Paita;
import interFacet.Tehdas;

public class Jasper {

	Kengat keng�t = null;
    Lippis lippis = null;
    Paita paita = null;
    Housut housut = null;
    
    public Jasper(Tehdas tehdas) {
    	housut = tehdas.luoHousut();
        keng�t = tehdas.luoKengat();
        lippis = tehdas.luoLippis();
        paita = tehdas.luoPaita();
    }
    public void kerroVaatteet(){
        System.out.println("Minulla on p��ll�: " + housut + ", " + keng�t + ", " + lippis + " ja " + paita);
    }
}
