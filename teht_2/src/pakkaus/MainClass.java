package pakkaus;

import interFacet.Tehdas;

public class MainClass {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Tehdas tehdas = null;
        Class luokka = null;
        
//        luokka = Class.forName("pakkaus.BossTehdas");
        luokka = Class.forName("pakkaus.AdidasTehdas");
        tehdas = (Tehdas) luokka.newInstance();
        
        Jasper jasper = new Jasper(tehdas);
        jasper.kerroVaatteet();
    }
}
