package pakkaus;

import interFacet.Housut;
import interFacet.Kengat;
import interFacet.Lippis;
import interFacet.Paita;
import interFacet.Tehdas;

public class BossTehdas implements Tehdas{

	public Kengat luoKengat() {
        return new KengatB();
    }
    public Housut luoHousut() {
        return new HousutB();
    }
    public Paita luoPaita() {
        return new PaitaB();
    }
    public Lippis luoLippis() {
        return new LippisB();
    }
}
