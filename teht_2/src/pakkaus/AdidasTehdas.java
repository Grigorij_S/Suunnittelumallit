package pakkaus;

import interFacet.Housut;
import interFacet.Kengat;
import interFacet.Lippis;
import interFacet.Paita;
import interFacet.Tehdas;

public class AdidasTehdas implements Tehdas{

	public Kengat luoKengat() {
        return new KengatA();
    }
    public Housut luoHousut() {
        return new HousutA();
    }
    public Paita luoPaita() {
        return new PaitaA();
    }
    public Lippis luoLippis() {
        return new LippisA();
    }
    
}
