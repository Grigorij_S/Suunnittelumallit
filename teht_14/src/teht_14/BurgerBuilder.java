package teht_14;

public interface BurgerBuilder {
	
	public void printBurger();
	public void buildJuusto();
	public void buildKastike();
	public void buildLeipa();
	public void buildPihvi();
	public void buildSuolakurkku();

}
