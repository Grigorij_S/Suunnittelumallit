package teht_14;

import java.util.ArrayList;

import teht_14.ainesosat.Juusto;
import teht_14.ainesosat.Kastike;
import teht_14.ainesosat.Leipa;
import teht_14.ainesosat.Pihvi;
import teht_14.ainesosat.Suolakurkku;

public class Hesburger implements BurgerBuilder {

	ArrayList<Item> burgeri = new ArrayList<Item>();

	@Override
	public void printBurger() {
		System.out.print("Burgerissa on: ");
		for (Item item : burgeri) {
			System.out.print(item.itemName() + " ");
		}
	}

	@Override
	public void buildJuusto() {
		burgeri.add(new Juusto());
	}

	@Override
	public void buildKastike() {
		burgeri.add(new Kastike());
	}

	@Override
	public void buildLeipa() {
		burgeri.add(new Leipa());
	}

	@Override
	public void buildPihvi() {
		burgeri.add(new Pihvi());
	}

	@Override
	public void buildSuolakurkku() {
		burgeri.add(new Suolakurkku());
	}
}
