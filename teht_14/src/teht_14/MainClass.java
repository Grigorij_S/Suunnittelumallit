package teht_14;

public class MainClass {
	public static void main(String[] args) {
		
		Director dir = new Director();
		BurgerBuilder heseRakentaja = new Hesburger();
		BurgerBuilder mäkkiRakentaja = new McDonalds();
		
		dir.setBurgerBuilder(heseRakentaja);
		dir.constructBurger();
		dir.getBurger();
		
		System.out.println("");

		dir.setBurgerBuilder(mäkkiRakentaja);
		dir.constructBurger();
		dir.getBurger();
	}
}
