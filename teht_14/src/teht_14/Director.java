package teht_14;

public class Director {
	
	private BurgerBuilder rakentaja;
	
	public void setBurgerBuilder(BurgerBuilder rakentaja) {
		this.rakentaja = rakentaja;
	}
	public void constructBurger() {
		rakentaja.buildJuusto();
		rakentaja.buildKastike();
		rakentaja.buildLeipa();
		rakentaja.buildPihvi();
		rakentaja.buildSuolakurkku();
	}
	public void getBurger() {
		System.out.println(rakentaja.getClass().getSimpleName());
		rakentaja.printBurger();
	}
}
