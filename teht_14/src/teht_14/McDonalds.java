package teht_14;

import teht_14.ainesosat.Juusto;
import teht_14.ainesosat.Kastike;
import teht_14.ainesosat.Leipa;
import teht_14.ainesosat.Pihvi;
import teht_14.ainesosat.Suolakurkku;

public class McDonalds implements BurgerBuilder {

	String burgeri = "";

	@Override
	public void printBurger() {
		System.out.println("Burgerissa on: " + burgeri);
	}

	@Override
	public void buildJuusto() {
		burgeri = burgeri + new Juusto().itemName() + " ";

	}

	@Override
	public void buildKastike() {
		burgeri = burgeri + new Kastike().itemName() + " ";
	}

	@Override
	public void buildLeipa() {
		burgeri = burgeri + new Leipa().itemName() + " ";
	}

	@Override
	public void buildPihvi() {
		burgeri = burgeri + new Pihvi().itemName() + " ";
	}

	@Override
	public void buildSuolakurkku() {
		burgeri = burgeri + new Suolakurkku().itemName() + " ";
	}
}
