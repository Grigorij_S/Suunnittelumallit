package teht_10;

public class ChiefMan extends Palkka{
	
	private double alin = 1.02 * palkka;
	private double ylin = 1.05 * palkka;
	
	public void processRequest(KorotusPyynt� pyynt�) {
		if (pyynt�.getSumma() <= ylin && pyynt�.getSumma() > alin) {
			System.out.println("ChiefMan vahvistaa palkankorotuksen : " + pyynt�.getSumma()+" �");
		} else if (successor != null) {
			successor.processRequest(pyynt�);
		}
	}
}
