package teht_10;

public class EsiMan extends Palkka{
	
	private double sallittu = 1.02 * palkka;
	
	public void processRequest(KorotusPyynt� pyynt�) {
		if (pyynt�.getSumma() <= palkka) {
			System.out.println("Palkkasi on jo suurempi...");
		} else if (pyynt�.getSumma() <= sallittu) {
			System.out.println("Esimies vahvistaa palkankorotuksen : " + pyynt�.getSumma()+" �");
		} else if (successor != null) {
			successor.processRequest(pyynt�);
		}
	}
}
