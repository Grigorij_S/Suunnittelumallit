package teht_10;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MainClass {
	// EsiMan -> ChiefMan -> BossMan
	
	public static void main(String[] args) {
        EsiMan esimies = new EsiMan();
        ChiefMan yksikönPäällikkö = new ChiefMan();
        BossMan pomo = new BossMan();
        
        esimies.setSuccessor(yksikönPäällikkö);
        yksikönPäällikkö.setSuccessor(pomo);
        
        System.out.println("Palkkasi on " + Palkka.palkka + "€");
		try {
			while (true) {
				System.out.println("Uusi palkkatoiveesi.");
				System.out.print(">");
				double pyyntö = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
				esimies.processRequest(new KorotusPyyntö(pyyntö));
			}
		} catch (Exception e) {
			System.exit(1);
		}
    }

}
