package teht_10;

public class KorotusPyyntö {

	private double summa;

	public KorotusPyyntö(double summa) {
		this.summa = summa;
	}

	public void setSumma(double summa) {
		this.summa = summa;
	}

	public double getSumma() {
		return summa;
	}

}
