package teht_10;

public abstract class Palkka {

	protected static double palkka = 2000;
	protected Palkka successor;
	
	public void setSuccessor(Palkka successor) {
		this.successor = successor;
	}
	abstract public void processRequest(KorotusPyynt� pyynt�);
}
