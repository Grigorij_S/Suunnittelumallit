package teht_10;

public class BossMan extends Palkka{
	
	private double sallittu = 1.05 * palkka;
	
	public void processRequest(KorotusPyynt� pyynt�) {
		if (pyynt�.getSumma() > sallittu) {
			System.out.println("BossMan vahvistaa palkankorotuksen : " + pyynt�.getSumma()+" �");
		} else if (successor != null) {
			successor.processRequest(pyynt�);
		}
	}
}
