package pakkaus;

public class MainClass {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		BossTehdas bt = BossTehdas.getInstance();
		AdidasTehdas at = AdidasTehdas.getInstance();
        
        Jasper AdidasJasper = new Jasper(at);
        AdidasJasper.kerroVaatteet();
        
        Jasper BossJasper = new Jasper(bt);
        BossJasper.kerroVaatteet();
    }
}
