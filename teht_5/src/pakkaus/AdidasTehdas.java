package pakkaus;

import interFacet.Housut;
import interFacet.Kengat;
import interFacet.Lippis;
import interFacet.Paita;
import interFacet.Tehdas;

public class AdidasTehdas implements Tehdas{
	
	private AdidasTehdas() {
	};
	
	private static AdidasTehdas firstInstance = null;
	
	public static synchronized AdidasTehdas getInstance() {
		if (firstInstance == null) {
			firstInstance = new AdidasTehdas();
		}
		return firstInstance;
	}

	public Kengat luoKengat() {
        return new KengatA();
    }
    public Housut luoHousut() {
        return new HousutA();
    }
    public Paita luoPaita() {
        return new PaitaA();
    }
    public Lippis luoLippis() {
        return new LippisA();
    }
    
}
