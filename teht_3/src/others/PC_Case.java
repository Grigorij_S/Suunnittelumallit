package others;

import java.util.ArrayList;
import interFacet.Component;

public class PC_Case implements Component{

	ArrayList<Component> compList = new ArrayList<Component>();
	
	double price = 50.00;
	
	public double getPrice(Component comp){
		for(Component c : compList){
			price = price + c.getPrice(comp);
		}
		return price;
	}
	
	public void addComponent(Component comp){
		compList.add(comp);
	}

}
