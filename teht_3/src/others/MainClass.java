package others;

import interFacet.Component;
import komponentit.CPU;
import komponentit.GPU;
import komponentit.PSU;
import komponentit.RAM;

public class MainClass {
	public static void main(String[] args){
		
		Component pcCase = new PC_Case();
		Component mBoard = new MotherBoard();
		Component cpu = new CPU();
		Component gpu = new GPU();
		Component psu = new PSU();
		Component ram = new RAM();
		
		mBoard.addComponent(gpu);
		mBoard.addComponent(cpu);
		mBoard.addComponent(ram);
		pcCase.addComponent(mBoard);
		
		pcCase.addComponent(psu);
		
		
		//can't add those
		ram.addComponent(cpu);
		gpu.addComponent(ram);
		psu.addComponent(gpu);
		
		
		System.out.println("Your build price is "+ pcCase.getPrice(pcCase) + "�");
		
	}
}
