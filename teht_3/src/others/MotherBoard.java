package others;

import java.util.ArrayList;
import interFacet.Component;

public class MotherBoard implements Component{

	ArrayList<Component> compList = new ArrayList<Component>();
	
	double price = 89.99;
	
	public double getPrice(Component comp){
		for(Component c : compList){
			price = price + c.getPrice(comp);
		}
		return price;
	}
	
	public void addComponent(Component comp){
		compList.add(comp);
	}
}
