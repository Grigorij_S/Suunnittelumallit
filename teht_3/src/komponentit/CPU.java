package komponentit;

import interFacet.Component;

public class CPU implements Component{
	double price = 399.99;
	
	public double getPrice(Component comp){
		return price;
	}
	
	public void addComponent(Component comp){
		System.out.println("Can't add "+comp.getClass().getSimpleName()+" to "+ this.getClass().getSimpleName()+".");
	}
}
