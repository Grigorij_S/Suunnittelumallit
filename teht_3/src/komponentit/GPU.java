package komponentit;

import interFacet.Component;

public class GPU implements Component{
	double price = 600.00;
	
	public double getPrice(Component comp){
		return price;
	}
	
	public void addComponent(Component comp){
		System.out.println("Can't add "+comp.getClass().getSimpleName()+" to "+ this.getClass().getSimpleName()+".");
	}
	
}
