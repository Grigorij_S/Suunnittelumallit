package interFacet;

public interface Component {
	public double getPrice(Component comp);
	public void addComponent(Component comp);
}
